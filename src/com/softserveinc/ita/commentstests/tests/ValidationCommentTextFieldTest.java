package com.softserveinc.ita.commentstests.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.softserveinc.ita.commentstests.criterias.Specification;
import com.softserveinc.ita.commentstests
        .domain.repositories.CommentsRepository;
import com.softserveinc.ita.commentstests.pages.EditPage;
import com.softserveinc.ita.commentstests.pages.MainPage;
import com.softserveinc.ita.commentstests.tools.WebDriverUtils;

/**
 * This class makes validation comment text field.
 * @author Andriy Lantukh
 *
 */
public class ValidationCommentTextFieldTest {
    /**
     * Specification object for soft-assert report create.
     */
    private Specification specification;
    /**
     * Main page object of Comments WebSite.
     */
    private MainPage mainPage;
    /**
     * Edit page object of Comments WebSite.
     */
    private EditPage editPage;
    /**
     * This method runs before test and
     * initialize global variables.
     */
    @BeforeClass
    public final void setUp() {
        WebDriverUtils.load(TestsConstants.URL_FOR_TEST);
        specification = Specification.get();
        mainPage = new MainPage();
        /* Preconditions */
        /* This section find and delete comment 1 */
        mainPage = mainPage
        .findComment(TestsConstants.NUM_OF_FIRST_COMMENT)
        .commentItemLineClick()
        .deleteButtonClick()
        .clickYes();
        /* This section find and delete comment 2 */
        mainPage = mainPage
                .findComment(TestsConstants.NUM_OF_SECOND_COMMENT)
                .commentItemLineClick()
                .deleteButtonClick()
                .clickYes();
    }
    /**
     * This method runs after test.
     */
    @AfterClass
    public final void tearDown() {
    //    WebDriverUtils.stop();
   //     System.out.println("Result of test-case execution:");
   //     System.out.println(specification.getDescription());
    }

    /**
     * This method is test for validation text field on Edit page.
     */
    @Test
    public final void doValidationCommentTextField() {
        specification
        /* This section opens edit page and try to create comment with
         * empty text field. */
        .For(editPage = mainPage.createNewCommentButtonClick())
        .next()
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1EmptyTextField()).doSave())
        .next()
         /* This section makes validation Error label. */
        .For(editPage.getValidationLabel())
        .isVisible()
        .valueMatch(TestsConstants.ERROR_LABEL_EMPTY_TEXT_FIELD)
        .next()
         /* This section check the text field is highlighted. */
        .For(editPage.getTextField())
        .colorMatch(TestsConstants.TEXT_FIELD_STATE)
        .next()
         /* This section check that all fields cleared after "Save". */
        .For(editPage.getCommentData())
        .commentMatch(CommentsRepository.getCommentEmptyAllFields());

         /* This section refill all fields. Text field is "a". And save it */
        specification
        .For(mainPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1AtextField()).successfulSaveReturn())
        .next()
        /* This section search for created comment and compare it */
        .For(mainPage.findComment(TestsConstants.NUM_OF_FIRST_COMMENT)
                .getComment())
        .commentMatch(CommentsRepository.getCommentNumber1AtextField());
        /* This delete comment 1 */
        mainPage = mainPage
                .findComment(TestsConstants.NUM_OF_FIRST_COMMENT)
                .commentItemLineClick()
                .deleteButtonClick()
                .clickYes();

        /* This section opens edit page and try to create comment with
         * 50 symbols in text field. */
        specification
        .For(editPage = mainPage.createNewCommentButtonClick())
        .next()
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1_50SymbolsTextField()).doSave())
        .next()
         /* This section check that all fields cleared after "Save". */
        .For(editPage.getCommentData())
        .commentMatch(CommentsRepository.getCommentEmptyAllFields())
        .next()
        /* This section search for created comment and compare it */
        .For(mainPage = editPage.quitByReturn())
        .next()
        .For(mainPage.findComment(TestsConstants.NUM_OF_FIRST_COMMENT)
                .getComment())
        .commentMatch(CommentsRepository
                .getCommentNumber1_50SymbolsTextField());
        /* This delete comment 1 */
        mainPage = new MainPage();
        mainPage = mainPage
                .findComment(TestsConstants.NUM_OF_FIRST_COMMENT)
                .commentItemLineClick()
                .deleteButtonClick()
                .clickYes();

        /* This section opens edit page and try to create comment with
         * 51 symbols in text field. */
        specification
        .For(editPage = mainPage.createNewCommentButtonClick())
        .next()
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1_51SymbolsTextField()).doSave())
        .next()
         /* This section check that all fields cleared after "Save". */
        .For(editPage.getCommentData())
        .commentMatch(CommentsRepository.getCommentEmptyAllFields())
        .next()
        /* This section makes validation Error label. */
        .For(editPage.getValidationLabel())
        .isVisible()
        .valueMatch(TestsConstants.ERROR_LABEL_MORE50_TEXT_FIELD)
        .next()
         /* This section check the text field is highlighted. */
        .For(editPage.getTextField())
        .colorMatch(TestsConstants.TEXT_FIELD_STATE);

        /* This section try to create comment with
         * special symbols in text field. */
        specification
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1SpecialSymbolsTextField()).doSave())
        .next()
         /* This section check that all fields cleared after "Save". */
        .For(editPage.getCommentData())
        .commentMatch(CommentsRepository.getCommentEmptyAllFields())
        .next()
        /* This section makes validation Error label. */
        .For(editPage.getValidationLabel())
        .isVisible()
        .valueMatch(TestsConstants.ERROR_LABEL_SPECIAL_SYMBOL_TEXT_FIELD)
        .next()
         /* This section check the text field is highlighted. */
        .For(editPage.getTextField())
        .colorMatch(TestsConstants.TEXT_FIELD_STATE);

        /* This section opens edit page and try to create comment
        * number 1. */
        specification
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber1()).doSave())
        .next()
        .For(editPage = editPage.setCommentData(CommentsRepository
                .getCommentNumber2WithTextField1()).doSave())
        .next()
        /* This section makes validation Error label. */
        .For(editPage.getValidationLabel())
        .isVisible()
        .valueMatch(TestsConstants.ERROR_LABEL_SAME_TEXT_IN_TWO_COMMENTS)
        .next()
         /* This section check the text field is highlighted. */
        .For(editPage.getTextField())
        .colorMatch(TestsConstants.TEXT_FIELD_STATE)
        .next()
        .check();
    }
}
